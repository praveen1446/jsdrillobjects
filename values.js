function sampleValues(testObject) {
    if(typeof testObject==='object' && testObject!==null){
        let valuesArray = [];
        for (let property in testObject) {
            valuesArray.push(testObject[property]);
        }
        return valuesArray;
    }
    else{
        return {};
    }
}
module.exports = sampleValues;