function samplePairs(testObject){
    if(typeof testObject==='object' && testObject!==null){
        let allPairs=[];
        for(let property in testObject){
            let currPair=[];
            currPair.push(property);
            currPair.push(testObject[property]);
            allPairs.push(currPair);
        }
        return allPairs;
    }
    else{
        return {};
    }
}
module.exports=samplePairs;
