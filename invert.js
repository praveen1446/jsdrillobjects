function sampleInvert(testObject){
    if(typeof testObject==='object' && testObject!==null){
        let invertObject={};
        for(let property in testObject){
            invertObject[testObject[property]]=property;
        }
        return invertObject;
    }
    else{
        return {};
    }
}
module.exports=sampleInvert;