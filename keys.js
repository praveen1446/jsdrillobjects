function sampleKeys(testObject){
    if(typeof testObject==='object' && testObject!==null){
        let keysArray=[];
        for(let property in testObject){
            keysArray.push(property);
        }
        return keysArray;
    }
    else{
        return {};
    }
}
module.exports=sampleKeys;