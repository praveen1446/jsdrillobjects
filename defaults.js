function sampleDefaults(testObject,newObject){
    if(typeof testObject==='object' && testObject!==null){
        let defaultObject={};
        for(let property in testObject){
            defaultObject[property]=testObject[property];
        }
        //console.log(defaultObject);
        for(let property in newObject){
            if(!defaultObject[property]){
                defaultObject[property]=newObject[property];
            }
        }
        return defaultObject;
    }
    else{
        return {};
    }
}
module.exports=sampleDefaults;