function sampleMapObject(testObject,callback){
    if(typeof testObject==='object' && testObject!==null){
        let mapObject={};
        for(let property in testObject){
            mapObject[property]=callback(testObject[property]);
        }
        return mapObject;
    }
    else{
        return {};
    }
}
module.exports=sampleMapObject;